﻿namespace Astral.Configuration.Settings
{
    public class ServiceOwner : Fact<string>
    {
        public ServiceOwner(string value) : base(value)
        {
        }
    }
}