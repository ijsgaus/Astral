﻿namespace Astral.Configuration.Settings
{
    public sealed class EndpointName : Fact<string>
    {
        public EndpointName(string value) : base(value)
        {
        }
    }
}