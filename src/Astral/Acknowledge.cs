﻿namespace Astral
{
    public enum Acknowledge
    {
        Ack,
        Nack,
        Requeue
    }
}