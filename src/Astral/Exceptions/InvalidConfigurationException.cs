﻿using System;

namespace Astral.Exceptions
{
    public class InvalidConfigurationException : PermanentException
    {
        public InvalidConfigurationException()
        {
        }

        public InvalidConfigurationException(string message) : base(message)
        {
        }

        public InvalidConfigurationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}