﻿namespace Astral.Configuration.Settings
{
    public sealed class IsResponseChannel : Fact<bool>
    {
        public IsResponseChannel(bool value) : base(value)
        {
        }
    }
}