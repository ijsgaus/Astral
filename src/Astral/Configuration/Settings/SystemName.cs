﻿namespace Astral.Configuration.Settings
{
    public sealed class SystemName : Fact<string>
    {
        public SystemName(string value) : base(value)
        {
        }
    }
}