﻿using System;

namespace Astral.Configuration.Settings
{

    public delegate Acknowledge ExceptionToAcknowledge(Exception ex);
}