﻿namespace Astral.Configuration
{
    public enum EndpointType
    {
        Event,
        Call
    }
}